﻿using UnityEngine;
using System.Collections;

public class ManualWeight : MonoBehaviour {
	public SkinnedMeshRenderer skin;
	public GameObject shapeKeyObject;
	public float SpineSkinValue = 0;

	void Start () {
		skin = shapeKeyObject.GetComponent<SkinnedMeshRenderer> ();
	}

	void Update () {
		Debug.Log(skin.GetBlendShapeWeight(3));
		skin.SetBlendShapeWeight(3,SpineSkinValue);
	}
}
