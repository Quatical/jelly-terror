﻿using UnityEngine;
using System.Collections;

public class SpawnNode : MonoBehaviour
{
	public Mesh SpawnMesh;
	public float SpawnOffset;
	public Vector3 SpawnPoint {
		get
		{
			Vector3 pos = transform.position;
			return new Vector3(pos.x, pos.y + SpawnOffset, pos.z);
		}
	}
	public Quaternion SpawnRotation {
		get
		{
			return transform.rotation;
		}
	}

	void Reset()
	{
		SpawnOffset = 0.2f;
	}

	void OnDrawGizmos() {
		Gizmos.color = new Color(1.0f, 1.0f, 0.0f);
		Gizmos.DrawWireMesh(SpawnMesh, transform.position);
		Gizmos.color = new Color(1.0f, 0.0f, 0.0f);
		Gizmos.DrawWireSphere(SpawnPoint, 0.1f);
	}
}
