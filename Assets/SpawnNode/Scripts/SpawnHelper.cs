﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnHelper
{
	private SpawnNode[] spawnNodes;
	private int index = 0;
	private Quaternion SpawnRotation;

	public SpawnHelper()
	{
		FindSpawnNodes();
	}

	private SpawnNode[] GetSpawnNodes(GameObject[] SpawnGameObjects)
	{
		int i = 0;
		SpawnNode[] spawnNodes = new SpawnNode[SpawnGameObjects.GetLength(0)];
		foreach (GameObject go in SpawnGameObjects) {
			spawnNodes[i] = go.GetComponent<SpawnNode>();
			i++;
		}
		return spawnNodes;
	}

	private void FindSpawnNodes()
	{
		SpawnNode tmp;
		spawnNodes = GetSpawnNodes(GameObject.FindGameObjectsWithTag("SpawnNode"));
		int spawnNodeCount = spawnNodes.GetLength(0);
		for (int i = 0; i < spawnNodeCount; i++) {
			int j = Mathf.FloorToInt(Random.Range(0, spawnNodeCount));
			tmp = spawnNodes[i];
			spawnNodes[i] = spawnNodes[j];
			spawnNodes[j] = tmp;
		}
	}

	public Vector3 GetSpawnPosition()
	{
		Vector3 position = spawnNodes[index].SpawnPoint;
		SpawnRotation = spawnNodes[index].SpawnRotation;
		index = (index + 1) % spawnNodes.GetLength(0);
		return position;
	}

	public Quaternion GetSpawnRotation()
	{
		return SpawnRotation;
	}
}
