﻿using UnityEngine;
using System.Collections;

public class UI_Inv_Parent : MonoBehaviour {

	public void DragSetParent(Transform ItemParent){
		Debug.Log ("Parent " + ItemParent);
		Debug.Log ("Reset Parent");
		this.transform.SetParent(ItemParent);
		Debug.Log ("Parent: "+this.transform.parent);
	}
}
