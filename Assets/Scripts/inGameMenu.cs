﻿using UnityEngine;
using System.Collections;

public class inGameMenu : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		this.GetComponent<Canvas>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)){
			print("escape key was pressed");
			if (this.GetComponent<Canvas>().enabled == false){
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
				this.GetComponent<Canvas>().enabled = true;
			} else {
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
				this.GetComponent<Canvas>().enabled = false;
			}
		}
	}
	
	public void QuitGame(){
		Debug.Log ("Leaving");
		GameObject.Find("Scripts").GetComponent<InGame>().QuitGame();
	}
}
