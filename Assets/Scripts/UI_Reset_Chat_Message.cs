﻿using UnityEngine;
using System.Collections;

public class UI_Reset_Chat_Message : MonoBehaviour {

	// Use this for initialization
	void Start () {
		float index = (float)(transform.GetSiblingIndex());
		float x = index*(-105f);
		Vector2 size = new Vector2 (x,-35);
		transform.GetChild(1).GetComponent<RectTransform>().anchoredPosition = size;
	}
	
	// Update is called once per frame
	void Update () {

	}
}
