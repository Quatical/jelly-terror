﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UI_Inv_Manager : MonoBehaviour {

	public Asset_Database _Asset_Database;

	//public Transform Char_Head;
	public Transform InsItem;
	public Vector3 InsItemPos;
	public List<Items> PlayerInv;
	public List<GameObject> icons;
	public Transform gDropZone;

	//START

	public void Begin(){
		PlayerInv = new List<Items> ();

		//REFERENCE attachments
		//Char_Head = GameObject.Find("ORG-head").transform;

		//REFERENCE scripts
		_Asset_Database = GameObject.Find("Asset_Database").GetComponent<Asset_Database>();
		gDropZone = GameObject.Find("gDropZone").transform;

		//IMPORT PLAYER INVENTORY - Cycle "Asset_Database" for items with tag "owned" and write to array
		FindItems("Weapons", false);
		FindItems("Hats", false);
		FindItems("Skins", false);
		FindItems("Serums", false);
		FindItems("Finishers", false);
		ActivateItems("Weapons");

		Debug.Log (PlayerInv.Count);

	}

	public void ActivateItems(string ItemType){
		PlayerInv = new List<Items> ();
		icons = new List<GameObject> ();
		int counter = this.transform.childCount;
		for (int i = 0; i<counter; i++){
			this.transform.GetChild(0).SetParent(gDropZone.FindChild(this.transform.GetChild(0).gameObject.name.Split('_')[0]));
		}
		FindItems(ItemType, true);
	}

	public void FindItems(string ItemType, bool Loaded){
		switch(ItemType){
		case "Weapons":
			foreach (Items Item in _Asset_Database.Weapons) {
				ItemCall(Item,Loaded);
			}
			break;
		case "Hats":
			foreach (Items Item in _Asset_Database.Hats) {
				ItemCall(Item,Loaded);
			}
			break;
		case "Skins":
			foreach (Items Item in _Asset_Database.Skins) {
				ItemCall(Item,Loaded);
			}
			break;
		case "Serums":
			foreach (Items Item in _Asset_Database.Serums) {
				ItemCall(Item,Loaded);
			}
			break;
		case "Finishers":
			foreach (Items Item in _Asset_Database.Finishers) {
				ItemCall(Item,Loaded);
			}
			break;
	}
	}

	public void ItemCall (Items Item, bool Loaded){
		if (Item.Owned == true){
			GameObject icon;
			if (!Loaded){
				icon = (Instantiate((GameObject)Resources.Load("Prefabs/Inventory_Item"), Vector3.zero, Quaternion.identity)) as GameObject;
				icon.name = Item.Name;
				icon.transform.parent = gDropZone.FindChild((Item.Name.Split('_')[0]));
			} else {
				icon = GameObject.Find(Item.Name);
				icon.transform.parent = this.transform;
				PlayerInv.Add (Item);
			}

			icons.Add(icon);
			//SET EQUIPPED ITEMS - Cycle "owned" for tag "equipped"
			if (Resources.Load("Sprites/Items/Icon_"+Item.Name) != null){
				Debug.Log ("Try Icon");
				icon.transform.GetChild(0).name = Item.Name+"_Image";
				icon.transform.GetChild(1).name = Item.Name+"_Equip";
				icon.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Items/Icon_"+Item.Name);
			} else {
				Debug.Log ("Default Icon");
				icon.transform.GetChild(0).name = Item.Name+"_Image";
				icon.transform.GetChild(1).name = Item.Name+"_Equip";
				icon.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Items/Icon_Default");
			}
			if (Item.Equipped == true){
				Debug.Log (Item.Prefab);
				icon.transform.GetChild(1).gameObject.SetActive(true);
				InstantiateItem(Item);
			} else {
				icon.transform.GetChild(1).gameObject.SetActive(false);
			}
		}
	}


	//INSTANTIATE ITEM

	public void InstantiateItem(Items Item){
		bool PassedChecks = false;
		Debug.Log (Item.Prefab);
		switch (Item.Name.Split('_')[0]){
		case "Hats":
			Debug.Log ("Got a Hat");
			InsItem = GameObject.Find("ORG-head").transform;
			if (InsItem.transform.childCount != 0){
				if (InsItem.transform.GetChild(0).name == Item.Name){
					PassedChecks = false;

				} else {
					foreach (Items oItem in _Asset_Database.Hats) {
						if (oItem == Item){
							oItem.Equipped = true;
						}else{
							oItem.Equipped = false;
						}
					}
					Destroy (InsItem.transform.GetChild(0).gameObject);
					PassedChecks = true;
				}
			} else {
				Item.Equipped = true;
				PassedChecks = true;
			}
			if (PassedChecks){
				PlayerPrefs.SetString("Hat",Item.Name);
			}
			InsItemPos = InsItem.position;
			break;
		case "Weapons":
			Debug.Log ("Got a Weapon");
			InsItem = GameObject.Find("ORG-hand_R").transform.FindChild("WeaponHolder");
			if (InsItem.transform.childCount != 0){
				if (InsItem.transform.GetChild(0).name == Item.Name){
					PassedChecks = false;
					
				} else {
					foreach (Items oItem in _Asset_Database.Weapons) {
						if (oItem.Name == Item.Name){
							oItem.Equipped = true;
						}else{
							oItem.Equipped = false;
						}
					}
					Destroy (InsItem.transform.GetChild(0).gameObject);
					PassedChecks = true;
				}
			} else {
				Item.Equipped = true;
				PassedChecks = true;
			}
			if (PassedChecks){
				PlayerPrefs.SetString("Weapon",Item.Name);
			}
			InsItemPos = InsItem.position;
			break;
		case "Serums":
			Debug.Log ("Got a Serums");
			foreach (Items oItem in _Asset_Database.Serums) {
				if (oItem.Name == Item.Name){
					oItem.Equipped = true;
				} else {
					oItem.Equipped = false;
				}
			}
			PlayerPrefs.SetString("Serum",Item.Name);
			break;
		case "Skins":
			Debug.Log ("Got a Skin");
			Transform Gummy = GameObject.Find("GummyB").transform.FindChild("Gummy_Bear1");
			Transform colour = Gummy.FindChild("Colours");
			PlayerPrefs.SetString("Skin",Item.Name);
			foreach (Items oItem in _Asset_Database.Skins) {
				if (oItem.Name == Item.Name){
					oItem.Equipped = true;
				}else{
					oItem.Equipped = false;
				}
			}
			switch(Item.Name){
			case "Skins_Orange":
				Debug.Log("ORANGE");
				Gummy.GetComponent<SkinnedMeshRenderer>().material = colour.FindChild("orange").GetComponent<SkinnedMeshRenderer>().material;
				break;
			case "Skins_Red":
				Debug.Log("RED");
				Gummy.GetComponent<SkinnedMeshRenderer>().material = colour.FindChild("red").GetComponent<SkinnedMeshRenderer>().material;
				break;
			case "Skins_Green":
				Debug.Log("GREEN");
				Gummy.GetComponent<SkinnedMeshRenderer>().material = colour.FindChild("green").GetComponent<SkinnedMeshRenderer>().material;
				break;
			case "Skins_Purple":
				Debug.Log("PURPLE");
				Gummy.GetComponent<SkinnedMeshRenderer>().material = colour.FindChild("purple").GetComponent<SkinnedMeshRenderer>().material;
				break;
			}
			break;
		case "Finishers":
			foreach (Items oItem in _Asset_Database.Finishers) {
				if (oItem.Name == Item.Name){
					oItem.Equipped = true;
				} else {
					oItem.Equipped = false;
				}
			}
			Debug.Log ("Got a Finisher");
			PlayerPrefs.SetString("Finisher",Item.Name);
			break;
		}
		if (PassedChecks == true){
			Debug.Log(Item.Prefab);
			GameObject gItem = (Instantiate(Item.Prefab, InsItemPos, Quaternion.identity)) as GameObject;
			gItem.transform.SetParent(InsItem);
			if (Item.Name.Split('_')[0] == "Hats"){
				gItem.transform.localRotation = Quaternion.Euler(0,180,270);
				gItem.transform.localPosition = Vector3.zero;
				gItem.transform.localScale = new Vector3 (1,1,1);
			} else {
				gItem.transform.localRotation = Quaternion.Euler(0,0,0);
			}
		}
	}
	


	//ASSIGNITEM

	public void AssignItem(GameObject Icon_Name){
		Debug.Log ("PlayerInv: "+PlayerInv.Count);
		foreach (Items item in PlayerInv) {
			if (item.Name == Icon_Name.name){
				foreach (GameObject icon in icons){
					if (icon.name == item.Name){
						icon.transform.GetChild(1).gameObject.SetActive(true);
					} else {
						icon.transform.GetChild(1).gameObject.SetActive(false);
					}
				}
				Debug.Log ("//AssignItem");
				InstantiateItem(item);
			}
		}
	}
}
