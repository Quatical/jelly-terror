﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerEntryComparer : IComparer<PlayerEntryController>
{
	public int Compare(PlayerEntryController x, PlayerEntryController y)
	{
		return (y.Info.KillCount - y.Info.DeathCount).CompareTo(x.Info.KillCount - x.Info.DeathCount);
	}
} 

public class LeaderBoardController : MonoBehaviour
{
	public float LeaderCamUpdateTime = 5;
	private float LeaderCamCooldown = 0;

	public GameObject leaderBoard;
	public GameObject playerEntry;
	
	private List<PlayerEntryController> controllers = new List<PlayerEntryController>();
	private Transform tableScrollContent;
	private Vector2 halfOffset;

	public GameObject LeadingPlayer;
	private PlayMakerFSM FollowFSM;
	
	void Start()
	{
		FollowFSM = GetComponent<PlayMakerFSM> ();
	}
	
	private void UpdateEntryEnvironment()
	{
		Transform tableScrollArea = leaderBoard.transform.FindChild("TableScrollArea");
		tableScrollContent = tableScrollArea.transform.FindChild("TableScrollContent");
		RectTransform tableRT = tableScrollContent.gameObject.GetComponent<RectTransform>();
		halfOffset = tableRT.rect.size/2f;
		halfOffset.x *= -1f;
	}
	
	public void RefreshEntries()
	{
		int i = 0;
		UpdateEntryEnvironment();
		controllers.Sort(new PlayerEntryComparer());
		LeadingPlayer = controllers [i].Bear;
		FollowFSM.Fsm.Event ("SetNewPlayer");

		foreach (PlayerEntryController controller in controllers) {
			RectTransform rectTransform = controller.PlayerEntry.GetComponent<RectTransform>();
			rectTransform.localPosition = halfOffset - new Vector2(0.0f, i*39.0f);
			i++;
		}
	}
	
	public void OnBearJoined(GameObject bear)
	{
		BogusPlayerInfo info = bear.GetComponent<BogusPlayerInfo>();
		if (!info)
			return;
		UpdateEntryEnvironment();
		GameObject entry = Instantiate(playerEntry, tableScrollContent.position, Quaternion.identity) as GameObject;
		if (tableScrollContent)
			entry.transform.SetParent(tableScrollContent, false);
		RectTransform rectTransform = entry.GetComponent<RectTransform>();
		rectTransform.localPosition = halfOffset - new Vector2(0.0f, controllers.Count*39.0f);
		PlayerEntryController entryController = new PlayerEntryController(bear, info, entry);
		controllers.Add(entryController);
	}
	
	public void OnBearQuit(GameObject bear)
	{
		PlayerEntryController entryController = null;
		for (int i = 0; i < controllers.Count; i++) {
			entryController = controllers[i] as PlayerEntryController;
			if (entryController.Bear == bear) {
				controllers.RemoveAt(i);
				break;
			}
		}
		if (entryController == null) {
			Debug.LogError("An unknown bear quit.");
			return;
		}
		entryController.Destroy();
		Object.Destroy(bear);
		RefreshEntries();
	}
	
	public void OnStatsChanged()
	{
		RefreshEntries();
	}
	
	void Update()
	{
		if (LeaderCamCooldown <= 0) {
			LeaderCamCooldown = LeaderCamUpdateTime;
			RefreshEntries ();
		} else {
			LeaderCamCooldown -= Time.deltaTime;
		}

		if (Input.GetButton("ShowLeaderBoard") && !leaderBoard.activeSelf) {
			RefreshEntries();
			leaderBoard.SetActive(true);
		}
		else if (!Input.GetButton("ShowLeaderBoard") && leaderBoard.activeSelf) {
			leaderBoard.SetActive(false);
		}
		
		foreach (PlayerEntryController controller in controllers) {
			controller.Update();
		}
	}
}
