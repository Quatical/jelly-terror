﻿using UnityEngine;
using System.Collections;
using System;

public class UI_Inv_Autosize : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		int rows = (int)(Math.Ceiling((double)this.transform.GetChild(0).transform.childCount/5));
		this.GetComponent<RectTransform>().SetHeight((rows*170)+20);
	}
}
