using UnityEngine;
using System.Collections;

public class ThirdPersonNetwork : Photon.MonoBehaviour
{
	private float OldPos = 0;

	private bool Dead = false;
	private bool IsMaster = false;
	PlayMakerFSM[] FSMs;
	//public PlayMakerFSM MovementFSM;
	//public PlayMakerFSM MouseLookFSM;
	public PlayMakerFSM CombatFSM;
	//public PlayMakerFSM CameraFSM;
	public PlayMakerFSM XpFSM;
	public Animator CharacterAnimator;
	private CharacterMovementController charController;
	private LimbSystem limbs;

	public int Kills = 0;
	public int Deaths = 0;

	public float AttackBlend = 0;
	public bool AttackTrigger;
	public bool DodgeTrigger;

	private Vector3 correctPlayerPos = Vector3.zero; //We lerp towards this
	private Quaternion correctPlayerRot = Quaternion.identity; //We lerp towards this

	//Movement animation state sync
	float Movement_Forward = 0;
	float Movement_Turn = 0;
	bool Movement_OnGround = true;
	//float Movement_Jump = 0;
	//float Movement_JumpLeg = 0;
	bool Movement_JumpUp = false;

    void Awake()
    {	
		limbs = GetComponent<LimbSystem> ();
		CharacterAnimator = GetComponent<Animator> ();
		charController = GetComponent<CharacterMovementController> ();

        //controllerScript = GetComponent<ThirdPersonController>();
		FSMs = GetComponents<PlayMakerFSM> ();

		foreach (PlayMakerFSM fsm in FSMs) {
			if (fsm.FsmName == "CombatFSM") {
				CombatFSM = fsm;
			} else if (fsm.FsmName == "XP-Check") {
				XpFSM = fsm;
			}
		}

         if (photonView.isMine)
        {
			IsMaster = PhotonNetwork.isMasterClient;
			Transform Orbit = transform.FindChild("rig").GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).FindChild("CameraTarget");
			Transform MainCam = Camera.main.transform;

			MainCam.GetComponent<SmoothFollowCopy>().SetTarget(Orbit);

			charController.enabled = true;
			CombatFSM.enabled = true;
			transform.FindChild("CanvusRoot").gameObject.SetActive(false);
			//PlayMakerFSM CameraFSM = MainCam.GetComponent<PlayMakerFSM>();

			//CameraFSM.FsmVariables.GetFsmGameObject("Target").Value = null;
			//CameraFSM.Fsm.Stop();
			//CameraFSM.Fsm.RestartOnEnable = true;
			//CameraFSM.enabled = true;
			//CameraFSM.Fsm.InitData();
			//CameraFSM.Fsm.Start();
			//MainCam.gameObject.SetActive(false);
			//CameraFSM.Fsm.RestartOnEnable = true;
			//CameraFSM.FsmVariables.GetFsmGameObject("Target").Value = this.gameObject;
			//MainCam.gameObject.SetActive(true);


			//MainCam.parent = Orbit;
			//MainCam.localPosition = Vector3.zero;
			//MainCam.rotation = Orbit.rotation;
            //MINE: local player, simply enable the local scripts
			//MovementFSM.enabled = true;
			//MouseLookFSM.enabled = true;
        }
        else
        {           
			CharacterAnimator.applyRootMotion = false;
			charController.enabled = false;
			CombatFSM.enabled = false;
			//MovementFSM.enabled = false;
        }

		transform.FindChild ("CanvusRoot").GetChild(0).GetComponent<Canvas> ().worldCamera = Camera.main;
		//Cursor.lockState = CursorLockMode.Confined;
		//Cursor.visible = false;

		transform.name = transform.name + photonView.viewID + " ("+ photonView.owner.name + ")";
		GetComponent<BogusPlayerInfo>().OnJoined();
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {	
            //We own this player: send the others our data
            //stream.SendNext((int)controllerScript._characterState);
			//WalkInt = CharacterAnimator.GetInteger("Movment");

			// Movement Animation Write
			Movement_Forward = CharacterAnimator.GetFloat("Forward");
			Movement_Turn = CharacterAnimator.GetFloat("Turn");
			Movement_OnGround = CharacterAnimator.GetBool("OnGround");
			//Movement_Jump = CharacterAnimator.GetFloat("Jump");
			//Movement_JumpLeg = CharacterAnimator.GetFloat("JumpLeg");
			Movement_JumpUp = CharacterAnimator.GetBool("JumpUp");


			stream.SendNext(Movement_Forward);
			stream.SendNext(Movement_Turn);
			stream.SendNext(Movement_OnGround);
			//stream.SendNext(Movement_Jump);
			//stream.SendNext(Movement_JumpLeg);
			stream.SendNext((float)AttackBlend);
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation); 
			stream.SendNext(Movement_JumpUp);
		}
		else
		{
            //Network player, receive data
            //controllerScript._characterState = (CharacterState)(int)stream.ReceiveNext();

			Movement_Forward = (float)stream.ReceiveNext();
			Movement_Turn = (float)stream.ReceiveNext();
			Movement_OnGround = (bool)stream.ReceiveNext();
			//Movement_Jump = (float)stream.ReceiveNext();
			//Movement_JumpLeg = (float)stream.ReceiveNext();
			AttackBlend = (float)stream.ReceiveNext();
			correctPlayerPos = (Vector3)stream.ReceiveNext();
			correctPlayerRot = (Quaternion)stream.ReceiveNext();
			Movement_JumpUp = (bool)stream.ReceiveNext();
			
			CharacterAnimator.SetFloat("Forward", Movement_Forward);
			CharacterAnimator.SetFloat("Turn", Movement_Turn);
			CharacterAnimator.SetBool("OnGround", Movement_OnGround);
			//CharacterAnimator.SetFloat("Jump", Movement_Jump);
			//CharacterAnimator.SetFloat("JumpLeg", Movement_JumpLeg);

			CharacterAnimator.SetFloat("Attack", AttackBlend);
			if (AttackTrigger) {
				CharacterAnimator.SetTrigger("AttackTrigger");
				AttackTrigger = false;
			}
			if (DodgeTrigger) {
				CharacterAnimator.SetTrigger("DodgeTrigger");
				DodgeTrigger = false;
			}
        }
    }

    void Update()
    {
		if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.LeftAlt)) {
			Cursor.visible = true;
		}

		/*if (!IsMaster && PhotonNetwork.isMasterClient && !photonView.isMine) {
			//DestroyCharacter();
			Debug.Log("MissMatch");
			PhotonNetwork.RemoveRPCs(this.photonView);
			PhotonNetwork.Destroy(this.photonView);
		}*/
        if (!photonView.isMine) {
			//CharacterAnimator.SetFloat("Attack", (float)AttackBlend);
			//CharacterAnimator.SetInteger ("Movment", (int)WalkInt);
			//Update remote player (smooth this, this looks good, at the cost of some accuracy)
			transform.position = Vector3.Lerp (transform.position, correctPlayerPos, Time.deltaTime * 5);
			transform.rotation = Quaternion.Lerp (transform.rotation, correctPlayerRot, Time.deltaTime * 5);
		} else {
			if (limbs.curHealth <= 0) {
				//Camera.main.gameObject.SetActive(false);
				//Camera.main.transform.parent = null;
				//DestroyCharacter();
				Die();
			}


			/*float NewPos = Input.GetAxis("Mouse X");
			float CalcPos = NewPos - OldPos;
			OldPos = NewPos;

			transform.Rotate(Vector3.up * (CalcPos * 10));*/
		}
    }

	void OnDestroy() {
		GetComponent<BogusPlayerInfo> ().OnQuit ();

		if (Dead) {
			GameObject.Find ("Scripts").GetComponent<InGame> ().SpawnPlayer ();
		}
	}

	public void OnLeftRoom() {
		PlayerPrefs.SetInt ("Player Xp" , (int)XpFSM.Fsm.GetFsmInt("xp").Value);
		PlayerPrefs.Save ();
	}

	public void OnDisconnectedFromPhoton() {
		PlayerPrefs.SetInt ("Player Xp" , XpFSM.Fsm.GetFsmInt("xp").Value);
		PlayerPrefs.Save ();
	}

	public void OnApplicationQuit() {
		PlayerPrefs.SetInt ("Player Xp" , XpFSM.Fsm.GetFsmInt("xp").Value);
		PlayerPrefs.Save ();
	}

	void Die() {
		Dead = true;
		PlayerPrefs.SetInt ("Player Xp" , XpFSM.Fsm.GetFsmInt("xp").Value);
		PlayerPrefs.Save ();

		//photonView.RPC ("RemoveCharacterStat", PhotonTargets.Others, new object[] { photonView.owner.name });
		//GetComponent<BogusPlayerInfo> ().OnQuit ();
		PhotonNetwork.Destroy(photonView);
	}
		
	[PunRPC]
	private void DestroyCharacterRPC (bool holder, PhotonMessageInfo info) {
		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.Destroy (photonView);
			//Destroy (gameObject);
		} else {
			//Destroy(gameObject);
		}
	}

	private void DestroyCharacter () {
		photonView.RPC("DestroyCharacterRPC", PhotonTargets.All, true);
	}

	public void SyncAttack () {
		photonView.RPC("AttackRPC", PhotonTargets.All, true);
	}

	public void SyncDodge () {
		photonView.RPC("DodgeRPC", PhotonTargets.All, true);
	}

	[PunRPC]
	private void AttackRPC (bool holder, PhotonMessageInfo info) {
		if (!photonView.isMine) {
			AttackTrigger = true;
		}
	}

	[PunRPC]
	private void DodgeRPC (bool holder, PhotonMessageInfo info) {
		if (!photonView.isMine) {
			DodgeTrigger = true;
		}
	}
}