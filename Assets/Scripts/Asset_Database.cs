﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Items
{
	public GameObject Prefab { get; set; }
	public string Name { get; set; }
	public bool Owned { get; set; }
	public bool Equipped { get; set; }
	
}
public class Meta
{
	public Shader Prefab { get; set; }
	public string Name { get; set; }
	public bool Owned { get; set; }
	public bool Equipped { get; set; }
	
}

public class Asset_Database : MonoBehaviour {


	public List<Items> Weapons;
	public List<Items> Hats;
	public List<Items> Skins;
	public List<Meta> SkinsM;
	public List<Items> Serums;
	public List<Items> Finishers;

	// Use this for initialization
	void Awake () {

		Weapons = new List<Items> ();
		Weapons.Add (new Items () {Prefab=Resources.Load("Models/Items/Weapons/Weapon_SuckerMace") as GameObject, Name="Weapons_Mace", Owned=true, Equipped=false});
		Weapons.Add (new Items () {Prefab=Resources.Load("Models/Items/Weapons/Weapon_Trident") as GameObject, Name="Weapons_Trident", Owned=true, Equipped=false});
		Weapons.Add (new Items () {Prefab=Resources.Load("Models/Items/Weapons/Weapon_Sword") as GameObject, Name="Weapons_Sword", Owned=true, Equipped=false});
		//Weapons.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Beanie") as GameObject, Name="Weapons_Whip", Owned=true, Equipped=false});

		Hats = new List<Items> ();
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Xmas") as GameObject, Name="Hats_Xmas", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Spartan") as GameObject, Name="Hats_Spartan", Owned=true, Equipped=false});
		//Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Axe") as GameObject, Name="Hats_Axe", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Beanie") as GameObject, Name="Hats_Beanie", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Bowler") as GameObject, Name="Hats_Bowler", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Conductor") as GameObject, Name="Hats_Conductor", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Cowboy") as GameObject, Name="Hats_Cowboy", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Gasmask2") as GameObject, Name="Hats_Gasmask", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Halo") as GameObject, Name="Hats_Halo", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Headphones") as GameObject, Name="Hats_Headphones", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Top") as GameObject, Name="Hats_Top", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Trucker") as GameObject, Name="Hats_Trucker", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Viking") as GameObject, Name="Hats_Viking", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Cupcake") as GameObject, Name="Hats_Cupcake", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Horn") as GameObject, Name="Hats_Horn", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Propeller") as GameObject, Name="Hats_Propeller", Owned=true, Equipped=false});
		Hats.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Umbrella") as GameObject, Name="Hats_Umbrella", Owned=true, Equipped=false});

		SkinsM = new List<Meta> ();
		SkinsM.Add (new Meta () {Prefab=Resources.Load("Materials/Gummy_orange") as Shader, Name="Skins_Orange", Owned=true, Equipped=false});
		SkinsM.Add (new Meta () {Prefab=Resources.Load("Materials/Gummy_red") as Shader, Name="Skins_Red", Owned=true, Equipped=false});
		SkinsM.Add (new Meta () {Prefab=Resources.Load("Materials/Gummy_green") as Shader, Name="Skins_Green", Owned=true, Equipped=false});
		SkinsM.Add (new Meta () {Prefab=Resources.Load("Materials/Gummy_purple") as Shader, Name="Skins_Purple", Owned=true, Equipped=false});

		Skins = new List<Items> ();
		Skins.Add (new Items () {Prefab=new GameObject(), Name="Skins_Orange", Owned=true, Equipped=false});
		Skins.Add (new Items () {Prefab=new GameObject(), Name="Skins_Red", Owned=true, Equipped=false});
		Skins.Add (new Items () {Prefab=new GameObject(), Name="Skins_Green", Owned=true, Equipped=false});
		Skins.Add (new Items () {Prefab=new GameObject(), Name="Skins_Purple", Owned=true, Equipped=false});

		Serums = new List<Items> ();
		Serums.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Xmas") as GameObject, Name="Serums_Coffee", Owned=true, Equipped=false});
		Serums.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Spartan") as GameObject, Name="Serums_SourBomb", Owned=true, Equipped=false});
		Serums.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Axe") as GameObject, Name="Serums_Bubblegum", Owned=true, Equipped=false});
		Serums.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Beanie") as GameObject, Name="Serums_Fireball", Owned=true, Equipped=false});

		Finishers = new List<Items> ();
		Finishers.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Xmas") as GameObject, Name="Finishers_Jawbreaker", Owned=true, Equipped=false});
		Finishers.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Spartan") as GameObject, Name="Finishers_Fireballs", Owned=true, Equipped=false});
		Finishers.Add (new Items () {Prefab=Resources.Load("Models/Items/Hats/Hat_Axe") as GameObject, Name="Finishers_MoltenChoc", Owned=true, Equipped=false});

	}

	// Update is called once per frame
	void Update () {
	
	}
}
