﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharStats : MonoBehaviour {

	public Asset_Database _Asset_Database;
	public string Playername;
	public int Level;
	public int Kills;
	public string Hat;
	public string Weapon;
	public string Serum;
	public string Skin;
	public string Finisher;
	
	// Use this for initialization
	void Start () {
		_Asset_Database = GameObject.Find("Asset_Database").GetComponent<Asset_Database>();
		Playername = PlayerPrefs.GetString("username"); 
		Level = PlayerPrefs.GetInt("level");
		Kills = PlayerPrefs.GetInt("TotalKills");
		Hat = PlayerPrefs.GetString("Hat");
		Weapon = PlayerPrefs.GetString("Weapon");
		Serum = PlayerPrefs.GetString("Serum");
		Skin = PlayerPrefs.GetString("Skin");
		Finisher = PlayerPrefs.GetString("Finisher");
		GameObject.Find ("PlayerKillsText").GetComponent<Text>().text = Kills.ToString();
		GameObject.Find ("PlayerLevelText").GetComponent<Text>().text = Level.ToString();
		GameObject.Find ("PlayerUsernameText").transform.parent.GetComponent<InputField>().text = Playername;

		foreach (Items Item in _Asset_Database.Hats) {
			if (Item.Name == Hat){
				Item.Equipped = true;
			} else {
				Item.Equipped = false;
			}
		}
		foreach (Items Item in _Asset_Database.Weapons) {
			if (Item.Name == Weapon){
				Item.Equipped = true;
			} else {
				Item.Equipped = false;
			}
		}
		foreach (Items Item in _Asset_Database.Serums) {
			if (Item.Name == Serum){
				Item.Equipped = true;
			} else {
				Item.Equipped = false;
			}
		}
		foreach (Items Item in _Asset_Database.Skins) {
			if (Item.Name == Skin){
				Item.Equipped = true;
			} else {
				Item.Equipped = false;
			}
		}
		foreach (Items Item in _Asset_Database.Finishers) {
			if (Item.Name == Finisher){
				Item.Equipped = true;
			} else {
				Item.Equipped = false;
			}
		}

	}
	
	public void setUsername(){
		PlayerPrefs.SetString("username", GameObject.Find ("PlayerUsernameText").transform.parent.GetComponent<InputField>().text);
	}
}
