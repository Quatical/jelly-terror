﻿using UnityEngine;
using System.Collections;

public class UI_Server_Entry : MonoBehaviour {

	public UI_Server_Manager _UI_Server_Manager;
	public UI_Manager _UI_Manager;

	// Use this for initialization
	public void ActivateElement () {
		_UI_Manager = GameObject.Find ("UI_Manager").GetComponent<UI_Manager> ();
		_UI_Server_Manager = GameObject.Find("UI_Server_Manager").GetComponent<UI_Server_Manager>();
//		if (_UI_Server_Manager.firstEntry == true){
//			_UI_Server_Manager.UpdateEntries();
//			_UI_Server_Manager.AddEntry(this.transform.gameObject);
////			_UI_Manager.AdjustScreen();
//			_UI_Server_Manager.firstEntry = false;
//		} else {
			_UI_Server_Manager.AddEntry(this.transform.gameObject);
//		}
	}
	public void UpdateServerEntry(){
		GameObject.Find ("MultiplayerScripts").GetComponent<ConnectToServer>().CurrentServerChoice = transform.name;
	}
}