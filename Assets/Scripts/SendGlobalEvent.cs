﻿using UnityEngine;
using System.Collections;

public class SendGlobalEvent : MonoBehaviour {
	public Animator Anim;
	public Transform Hand;
	public PlayMakerFSM Fsm;

	public bool mayHit = false;


	private Bullet bulletCollider;

	void Start () {
		/*if (Fsm.Fsm.GetFsmBool ("Sword").Value) {
			bulletCollider = Hand.FindChild("Weapon_Sword").FindChild("Collider").GetComponent<Bullet>();
		} else if (Fsm.Fsm.GetFsmBool ("Tridunt").Value) {
			bulletCollider = Hand.FindChild("Weapon_Trident").FindChild("Collider").GetComponent<Bullet>();
		} else if (Fsm.Fsm.GetFsmBool ("SuckerMace").Value) {
			bulletCollider = Hand.FindChild("Weapon_SuckerMace").FindChild("Collider").GetComponent<Bullet>();
		}*/
	}

	[PunRPC] 
	void SetWeaponChoiceRPC (int WeaponChoice, PhotonMessageInfo info) {
		switch (WeaponChoice) {
		case 0:
			Hand.FindChild("Weapon_Sword").gameObject.SetActive(true);
			break;
		case 1:
			Hand.FindChild("Weapon_Trident").gameObject.SetActive(true);
			break;
		case 2:
			Hand.FindChild("Weapon_SuckerMace").gameObject.SetActive(true);
			break;
		}
	}

	public void SetupTriggerController () {
		if (Fsm.Fsm.GetFsmBool ("Sword").Value) {
			bulletCollider = Hand.FindChild("Weapon_Sword").FindChild("Collider").GetComponent<Bullet>();
			GetComponent<PhotonView>().RPC("SetWeaponChoiceRPC", PhotonTargets.OthersBuffered, new object[] { 0 });
		} else if (Fsm.Fsm.GetFsmBool ("Tridunt").Value) {
			bulletCollider = Hand.FindChild("Weapon_Trident").FindChild("Collider").GetComponent<Bullet>();
			GetComponent<PhotonView>().RPC("SetWeaponChoiceRPC", PhotonTargets.OthersBuffered, new object[] { 1 });
		} else if (Fsm.Fsm.GetFsmBool ("SuckerMace").Value) {
			bulletCollider = Hand.FindChild("Weapon_SuckerMace").FindChild("Collider").GetComponent<Bullet>();
			GetComponent<PhotonView>().RPC("SetWeaponChoiceRPC", PhotonTargets.OthersBuffered, new object[] { 2 });
		}
	}

	void Update () {
		if (bulletCollider == null)
			return;

		mayHit = Anim.GetCurrentAnimatorStateInfo(0).IsName("AttackBlend");

		if (mayHit == true && bulletCollider.hit == false) {
			bulletCollider.mayHit = true;
		} else if (mayHit == false) {
			bulletCollider.hit = false;
			bulletCollider.mayHit = false;
		}
	}
}
