﻿using UnityEngine;
//using System.Collections;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class BogusPlayerInfo : MonoBehaviour
{
	public const string PlayerScoreProp = "score";
	public float UpdateCountdown = 1;
	private float Countdown = 0;

	private int ping;
	private PhotonView photonView;

	public LeaderBoardController Lbc;
	public string PlayerName;
	public int KillCount;
	public int DeathCount;

	public int Ping {
		get { return ping; }
	}

	void Reset()
	{
	PlayerName = "Anonymous";
	KillCount = GetScore (photonView.owner, "KillCount");
	DeathCount = GetScore (photonView.owner, "DeathCount");
	//Lbc = null;
	}

	public void OnQuit () {
		Lbc.OnBearQuit (gameObject);
	}

	public void OnJoined () {
		if (photonView == null) {
			photonView = GetComponent<PhotonView> ();
		}

		Lbc = GameObject.Find ("LeaderBoard").GetComponent<LeaderBoardController> ();
		PlayerName = photonView.owner.name;
		KillCount = GetScore (photonView.owner, "KillCount");
		DeathCount = GetScore (photonView.owner, "DeathCount");
		Lbc.OnBearJoined (gameObject);
	}

	public void UpdateScoresGlobally () {
		photonView.RPC ("UpdatePlayerScores", PhotonTargets.AllViaServer, new object[] {  });
	}

	public void AddKill (PhotonPlayer killer) {
		//photonView.RPC ("KillEventRPC", PhotonTargets.All, new object[] { killer.ID });
		int currentScore = GetScore (killer, "KillCount");
		currentScore = currentScore + 1;
		SetScore (killer, currentScore, "KillCount");
	}

	/*[PunRPC]
	void KillEventRPC (int ID, PhotonMessageInfo info) {
		if (ID == photonView.owner.ID) {
			int currentScore = GetScore (photonView.owner, "KillCount");
			currentScore = currentScore + 1;
			SetScore (photonView.owner, currentScore, "KillCount");
			KillCount = currentScore;
			UpdateScoresGlobally ();
		}
	}*/

	/*[PunRPC]
	void RemoveCharacterStat (string CharacterName, PhotonMessageInfo info) {
		Debug.Log ("Looking for leader board");
		GameObject[] Players = GameObject.FindGameObjectsWithTag ("Player");
		GameObject BearToRemove = null;
		foreach (GameObject player in Players) {
			if (player.name.Substring(23, player.name.Length-1) == CharacterName) {
				BearToRemove = player;
				break;
			}
		}
		GameObject.Find ("LeaderBoard").GetComponent<LeaderBoardController> ().OnBearQuit (BearToRemove);
		Debug.Log ("Done");
	}*/

	public void AddDeath () {
		DeathCount++;
		SetScore (photonView.owner, DeathCount, "DeathCount");
		UpdateScoresGlobally ();
	}

	[PunRPC]
	void UpdatePlayerScores () {
		DeathCount = GetScore (photonView.owner, "DeathCount");
		Lbc.OnStatsChanged();
	}

	public int GetScore(PhotonPlayer player, string ScoreName)
	{
		object value;
		if (player.customProperties.TryGetValue(ScoreName, out value))
		{
			return (int)value;
		}
		
		return 0;
	}

	public void SetScore(PhotonPlayer player, int NewScore, string ScoreName)
	{		
		Hashtable score = new Hashtable();
		score.Add (ScoreName, NewScore);
		player.SetCustomProperties (score);
	}

	void Update()
	{
		if (Countdown <= 0) {
			if (photonView.isMine) {
				ping = PhotonNetwork.GetPing ();
				SetScore (photonView.owner, ping, "Ping");
			} else {
				ping = GetScore (photonView.owner, "Ping");
			}
			KillCount = GetScore (photonView.owner, "KillCount");
			Countdown = UpdateCountdown;
		} else {
			Countdown -= Time.deltaTime;
		}
	}
}