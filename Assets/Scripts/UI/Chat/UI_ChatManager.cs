﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Messages
{
	public string Username { get; set; }
	public string Message { get; set; }
	
}

public class UI_ChatManager : MonoBehaviour {
	public List<List<Messages>> ChatDB;
	public int ActiveElement;
	Transform TabGroup;
	Transform MessageGroup;

	// Use this for initialization
	void Awake () {
		ChatDB = new List<List<Messages>>();
	}

	void Start(){
		TabGroup = GameObject.Find("Tab Area").transform;
		MessageGroup = GameObject.Find("Message Area").transform;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void newList(GameObject Username){
		bool exists = false;
		if (TabGroup.childCount != 0){
			for (int i = 0; i < TabGroup.childCount; i++){
				if (TabGroup.GetChild(i).GetComponent<playerID>().ID == Username.GetComponent<playerID>().ID){
					exists = true;
					LoadMessages(Username.transform);
				}
			}
		}
		if (exists == false){
			ChatDB.Add(new List<Messages>());
			GameObject chatTab = Instantiate(Resources.Load("prefabs/chatGroup") as GameObject);
			chatTab.GetComponent<playerID>().ID = Username.GetComponent<playerID>().ID;
			chatTab.transform.SetParent(TabGroup);
			chatTab.transform.GetChild(0).GetComponent<Text>().text = Username.transform.GetChild(2).GetChild(0).GetComponent<Text>().text;
			LoadMessages(chatTab.transform);
		}
	}

	public void newMessage(){
		string userName = PlayerPrefs.GetString("username");
		string message = transform.FindChild("InputField").GetComponent<InputField>().text;
		if (message != null || message != ""){
			ChatDB[ActiveElement].Add(new Messages () {Username = userName, Message = message});
			transform.FindChild("InputField").GetComponent<InputField>().text = "";
			GameObject messageG = Instantiate(Resources.Load("prefabs/chatMessage") as GameObject);
			messageG.transform.SetParent(MessageGroup);
			messageG.GetComponent<Text>().text = ChatDB[ActiveElement][ChatDB[ActiveElement].Count-1].Username + ": " + ChatDB[ActiveElement][ChatDB[ActiveElement].Count-1].Message;
		}
	}

	public void LoadMessages(Transform Active){
		ActiveElement = Active.GetSiblingIndex();
		int childCounter = MessageGroup.childCount;

		//Delete Previous
		for (int i = 0; i<childCounter;i++){
			DestroyImmediate(MessageGroup.GetChild(0).gameObject);
		}

//		foreach (Transform child in MessageGroup){
//			DestroyImmediate(child.gameObject);
//			Debug.Log ("Child Deleted");
//		}

		//Set Messages
		for (int i = 0; i < ChatDB[ActiveElement].Count; i++){
			GameObject message = Instantiate(Resources.Load("prefabs/chatMessage") as GameObject);
			message.transform.SetParent(MessageGroup);
			message.GetComponent<Text>().text = ChatDB[ActiveElement][i].Username + ": " + ChatDB[ActiveElement][i].Message;
			//Debug.Log ("New Message");
		}

		//Set Active Color
		foreach (Transform child in TabGroup){
			if (child.GetSiblingIndex() != ActiveElement){
				child.GetComponent<Image>().color = new Color(0.4f,0.4f,0.4f,0.4f);
			} else {
				child.GetComponent<Image>().color = new Color(1f,1f,1f,0.4f);
			}
		}

	}
	
}
