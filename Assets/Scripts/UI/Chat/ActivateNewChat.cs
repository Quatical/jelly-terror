﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActivateNewChat : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Button b = this.GetComponent<Button>();
		b.onClick.AddListener(() => Load ());
		//GameObject.Find("UI_Chat").GetComponent<UI_ChatManager>().newList();
	}

	void Load(){
		GameObject.Find("UI_Chat").GetComponent<UI_ChatManager>().LoadMessages(this.transform);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
