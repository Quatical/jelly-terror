﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActivateNewChatTab : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Button b = this.transform.GetChild(1).GetComponent<Button>();
		b.onClick.AddListener(() => Load ());
		//GameObject.Find("UI_Chat").GetComponent<UI_ChatManager>().newList();
	}

	void Load(){
		GameObject.Find("UI_Chat").GetComponent<UI_ChatManager>().newList(this.gameObject);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
