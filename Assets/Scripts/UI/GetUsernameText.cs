﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GetUsernameText : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.transform.GetComponent<Text>().text = transform.parent.transform.parent.transform.parent.transform.parent.transform.GetComponent<BogusPlayerInfo>().PlayerName;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
