﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerEntryController
{
  public GameObject Bear;
  public GameObject PlayerEntry;
  public BogusPlayerInfo Info;

  private Text playerName;
  private Text ping;
  private Text killCount;
  private Text deathCount;

  public PlayerEntryController(GameObject bear, BogusPlayerInfo info, GameObject playerEntry)
  {
    this.Bear = bear;
    this.Info = info;
    this.PlayerEntry = playerEntry;
    playerName = playerEntry.transform.Find("PlayerName").gameObject.GetComponent<Text>();
    ping = playerEntry.transform.Find("Ping").gameObject.GetComponent<Text>();
    killCount = playerEntry.transform.Find("KillCount").gameObject.GetComponent<Text>();
    deathCount = playerEntry.transform.Find("DeathCount").gameObject.GetComponent<Text>();
  }

  public void Update()
  {
    playerName.text = Info.PlayerName;
    ping.text = Info.Ping + "ms";
    killCount.text = Info.KillCount + "";
    deathCount.text = Info.DeathCount + "";
  }

  public void Destroy()
  {
    Object.Destroy(PlayerEntry);
  }
}
