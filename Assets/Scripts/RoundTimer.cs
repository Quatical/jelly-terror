﻿using UnityEngine;
using System.Collections;

public class RoundTimer : MonoBehaviour {
	// 5 Minute Match
	public float MatchRoundTime = 280f;
	public bool StartMatch = false;

	private float CountDown = 0;

	// Use this for initialization
	void Start () {
		CountDown = MatchRoundTime;
	}
	
	// Update is called once per frame
	void Update () {
		if (PhotonNetwork.isMasterClient) {
			if (StartMatch) {
				CountDown -= Time.deltaTime;
			}
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			stream.SendNext (CountDown);
		} else { 
			CountDown = (float)stream.ReceiveNext ();
		}
	}

	void StartRound () {
		GetComponent<PhotonView> ().RPC ("StartRoundRPC", PhotonTargets.AllViaServer, new object[] {  });
	}

	[PunRPC]
	void StartRoundRPC (PhotonMessageInfo info) {
		StartMatch = true;
	}
}
