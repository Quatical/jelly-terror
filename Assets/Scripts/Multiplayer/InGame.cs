using UnityEngine;

public class InGame : Photon.MonoBehaviour
{
    public Transform playerPrefab;
	private SpawnHelper helper;
	private Transform MainCamera;

	private GameObject LocalPlayer;

    public void Awake()
    {
        // in case we started this demo with the wrong scene being active, simply load the menu scene
        if (!PhotonNetwork.connected)
        {
            Application.LoadLevel(0);
            return;
        }

		MainCamera = Camera.main.transform;
		helper = new SpawnHelper ();
		PhotonNetwork.player.customProperties = new ExitGames.Client.Photon.Hashtable ();

        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
		SpawnPlayer ();
    }

	public void SpawnPlayer() {
		Vector3 pos = helper.GetSpawnPosition ();
		Quaternion rot = helper.GetSpawnRotation ();
		LocalPlayer = PhotonNetwork.Instantiate(this.playerPrefab.name, pos, rot, 0);
		//MainCamera.GetComponent<PlayMakerFSM> ().FsmVariables.GetFsmGameObject ("Target").Value = LocalPlayer.gameObject;
		//MainCamera.gameObject.SetActive (true);
	}

    public void QuitGame () 
    {
		int Kills = PlayerPrefs.GetInt("TotalKills");
		Kills += GetScore(PhotonNetwork.player, "KillCount");
		PlayerPrefs.SetInt("TotalKills", Kills);
		PhotonNetwork.RemoveRPCs(PhotonNetwork.player);
		PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player.ID);
        PhotonNetwork.LeaveRoom();  // we will load the menu level when we successfully left the room
    }

    public void OnMasterClientSwitched(PhotonPlayer player)
    {
        Debug.Log("OnMasterClientSwitched: " + player);

        //string message;
        InRoomChat chatComponent = GetComponent<InRoomChat>();  // if we find a InRoomChat component, we print out a short message

        if (chatComponent != null)
        {
            // to check if this client is the new master...
            if (player.isLocal)
            {
                //message = "You are Master Client now.";
            }
            else
            {
                //message = player.name + " is Master Client now.";
            }


            //chatComponent.AddLine(message); // the Chat method is a RPC. as we don't want to send an RPC and neither create a PhotonMessageInfo, lets call AddLine()

			LocalPlayer.GetComponent<BogusPlayerInfo>().OnQuit();
			PhotonNetwork.RemoveRPCs(PhotonNetwork.player);
			PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player.ID);
			PhotonNetwork.LeaveRoom();
        }
    }

    public void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom (local)");
        
        // back to main menu        
        Application.LoadLevel(0);
    }

    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("OnDisconnectedFromPhoton");

        // back to main menu        
        Application.LoadLevel(0);
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log("OnPhotonInstantiate " + info.sender);    // you could use this info to store this or react
    }

    public void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        Debug.Log("OnPhotonPlayerConnected: " + player);
    }

	public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        Debug.Log("OnPlayerDisconneced: " + player);
		PhotonNetwork.RemoveRPCs(player);
		PhotonNetwork.DestroyPlayerObjects(player);
	}

    public void OnFailedToConnectToPhoton()
    {
        Debug.Log("OnFailedToConnectToPhoton");

        // back to main menu        
        Application.LoadLevel(0);
    }

	public int GetScore(PhotonPlayer player, string ScoreName)
	{
		object value;
		if (player.customProperties.TryGetValue(ScoreName, out value))
		{
			return (int)value;
		}
		
		return 0;
	}
}
