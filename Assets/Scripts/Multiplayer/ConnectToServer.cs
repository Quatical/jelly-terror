﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConnectToServer : MonoBehaviour {
	private string[] names;

	public string ServerVersion = "1.0";
	public string PlayerName = "";
	public bool JoinedRoom = false;
	public RoomOptions myRoomSetting;
	public GameObject ListEntryPrefab;
	public GameObject ListObject;
	public string CurrentServerChoice;

	public byte MaxPlayers = 10;
	public string ServerName = "Untitled Server";

	void Awake () {
		//PhotonNetwork.ConnectUsingSettings ("1.0");
		PhotonNetwork.automaticallySyncScene = true;
		
		// the following line checks if this client was just created (and not yet online). if so, we connect
		if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated)
		{
			// Connect to the photon master-server. We use the settings saved in PhotonServerSettings (a .asset file in this project)
			PhotonNetwork.ConnectUsingSettings(ServerVersion);
		}
		
		// generate a name for this player, if none is assigned yet

		//PlayerName = "Guest" + Random.Range(1, 9999);
		//Debug.Log ("ILIKELOGS");
	}

	void Start () {
		TextAsset nameText = Resources.Load("names") as TextAsset;
		if (nameText)
			names = nameText.text.Split(new string[] {"\n"}, System.StringSplitOptions.RemoveEmptyEntries);

		if (PlayerPrefs.GetString ("Weapon") != "") {
			PlayerPrefs.SetString ("Weapon", "Weapons_Sword");
			PlayerPrefs.Save();
		}
	}

	void OnApplicationQuit () {
		PhotonNetwork.Disconnect ();
	}

	public void OnConnectionFail () {
		PhotonNetwork.ConnectUsingSettings(ServerVersion);
	}

	public void OnCreatedRoom()
	{
		PhotonNetwork.LoadLevel(1);
		Debug.Log("Created Room");
	}

	public void OnJoinedRoom()
	{
		Debug.Log("Joined Room");
	}

	public void OnPhotonCreateRoomFailed()
	{
		Debug.Log("Error: Can't create room (room name maybe already used).");
		Debug.Log("OnPhotonCreateRoomFailed got called. This can happen if the room exists (even if not visible). Try another room name.");
	}
	
	public void OnPhotonJoinRoomFailed(object[] cause)
	{
		Debug.Log("Error: Can't join room (full or unknown room name). " + cause[1]);
		Debug.Log("OnPhotonJoinRoomFailed got called. This can happen if the room is not existing or full or closed.");
	}
	public void OnPhotonRandomJoinFailed()
	{
		Debug.Log("Error: Can't join random room (none found).");
		Debug.Log("OnPhotonRandomJoinFailed got called. Happens if no room is available (or all full or invisible or closed). JoinrRandom filter-options can limit available rooms.");
	}

	public void SetServerName(GameObject textBox) {
		ServerName = textBox.GetComponent<InputField> ().text;
	}

	public void SetMaxPlayers(GameObject textBox) {
		MaxPlayers = byte.Parse( textBox.GetComponent<InputField> ().text);
	}

	public void RefreshServerList () {
		if (PhotonNetwork.connected) {
			RoomInfo[] list = PhotonNetwork.GetRoomList ();
			foreach (RoomInfo info in list) {
				if (ListObject.transform.FindChild(info.name) == null) {
					GameObject Obj = Instantiate (ListEntryPrefab, Vector3.zero, Quaternion.identity) as GameObject;
					Obj.transform.SetParent (ListObject.transform);
					Obj.name = info.name;
					Obj.GetComponent<RectTransform>().anchoredPosition = new Vector2 (0, Obj.GetComponent<RectTransform>().anchoredPosition.y);
					Obj.transform.FindChild("Name").GetChild(1).GetComponent<Text>().text = info.name;
					Obj.transform.FindChild("Players").GetChild(1).GetComponent<Text>().text = info.playerCount + " / " + info.maxPlayers;
					Obj.transform.FindChild("Ping").GetChild(1).GetComponent<Text>().text = PhotonNetwork.GetPing().ToString();
					Obj.GetComponent<UI_Server_Entry>().ActivateElement();
				}
			}
//			GameObject.Find("UI_Server_Manager").GetComponent<UI_Server_Manager>().UpdateEntries();
		} else {
			Debug.Log("Failed to conenct");
			Debug.Log(PhotonNetwork.connected);
		}
	}

	public void JoinARoom() {
		Debug.Log ("Joining room");
		if (CurrentServerChoice != "") {
			SetName ();
			PhotonNetwork.JoinRoom (CurrentServerChoice);
		} else {
			if (PhotonNetwork.countOfRooms != 0) {
				SetName();
				PhotonNetwork.JoinRandomRoom();
			}
		}
	}

	public void HostRoom() {
		if (PhotonNetwork.connected) {
			PhotonNetwork.autoCleanUpPlayerObjects = true;
			SetName();
			PhotonNetwork.CreateRoom(ServerName, new RoomOptions() { maxPlayers = MaxPlayers }, null);
		}
	}

	public void SetName() {
		if (PlayerPrefs.HasKey("username")) {
			string Username = PlayerPrefs.GetString("username");
			if (Username != "") {
				PhotonNetwork.playerName = PlayerPrefs.GetString("username");
			} else {
				PhotonNetwork.playerName = names[Mathf.FloorToInt(Random.Range(0, names.GetLength(0)))];
			}
		} else {
			PhotonNetwork.playerName = names[Mathf.FloorToInt(Random.Range(0, names.GetLength(0)))];
		}
	}
}
