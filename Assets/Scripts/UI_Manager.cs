﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_Manager : MonoBehaviour {

	public UI_Inv_Manager _UI_Inv_Manager;
	public GameObject UI_FG;
	public GameObject UI_BG;
	public GameObject UI_Join;
	public GameObject UI_Main;
	public GameObject UI_Loadout;
//	public GameObject UI_Options_Controls;
//	public GameObject UI_Options_Graphics;
//	public GameObject UI_Options_Audio;
	public GameObject GummyWalk;
	public GameObject ContentBG;

	public UI_Server_Manager _UI_Server_Manager;

//	public RectTransform gHost;
//	public RectTransform gServername;
//	public RectTransform gPlayers;
//	public RectTransform gGametype;
//	public RectTransform gMap;
//	public RectTransform gLimit;
//	public RectTransform gPing;
	public RectTransform gContainer;
	public Vector2 Container;
//	public Vector2 servername;
//	public Vector2 players;
//	public Vector2 gametype;
//	public Vector2 map;
//	public Vector2 limit;
//	public Vector2 ping;
//	Vector2 ScreenStore;
	bool first = true;

	//UI LINKS
	void Start(){

		GummyWalk = GameObject.Find("GummyB");
		GummyWalk.SetActive(false);
		ContentBG = GameObject.Find("UI_ContentBG");
		ContentBG.SetActive(false);
		_UI_Inv_Manager = GameObject.Find("UI_Inv_Manager").GetComponent<UI_Inv_Manager>();
		UI_FG=GameObject.Find("UI_FG");
		UI_BG=GameObject.Find("UI_BG");
		UI_Join=GameObject.Find("UI_Join");
		UI_Main=GameObject.Find("UI_Main");
		UI_Loadout=GameObject.Find("UI_Loadout");
		//_UI_Server_Manager=GameObject.Find("UI_Server_Manager").GetComponent<UI_Server_Manager>();
//		gHost=GameObject.Find("gHost").GetComponent<RectTransform>();
//		gServername=GameObject.Find("gServername").GetComponent<RectTransform>();
//		gPlayers=GameObject.Find("gPlayers").GetComponent<RectTransform>();
//		gGametype=GameObject.Find("gGametype").GetComponent<RectTransform>();
//		gMap=GameObject.Find("gMap").GetComponent<RectTransform>();
//		gLimit=GameObject.Find("gLock").GetComponent<RectTransform>();
//		gPing=GameObject.Find("gPing").GetComponent<RectTransform>();
//		gContainer=GameObject.Find("gContainer").GetComponent<RectTransform>();

		//UI_BG.SetActive(false);
		UI_FG.SetActive(true);
		UI_Main.SetActive(true);
		UI_Join.GetComponent<Canvas>().enabled = false;
		UI_Loadout.GetComponent<Canvas>().enabled = false;
//		UI_Options_Controls.SetActive(false);
//		UI_Options_Graphics.SetActive(false);
//		UI_Options_Audio.SetActive(false);


	}


	//MENU ACTIVATIONS

	public void MainMenu(GameObject Origin){
		Origin.SetActive(false);
		GummyWalk.SetActive(true);
		UI_Main.SetActive(true);
	}

	public void Join(GameObject Origin){
		if (Origin.name == "UI_Main"){
			Origin.SetActive(false);
		} else {
			Origin.GetComponent<Canvas>().enabled = false;
		}
		GummyWalk.SetActive(true);
		ContentBG.SetActive(true);
		UI_Join.GetComponent<Canvas>().enabled = true;
		if (first){
			_UI_Inv_Manager.Begin();
			first = false;
		}
	}

	public void Loadout(GameObject Origin){
		//GummyWalk.SetActive(false);
		Origin.GetComponent<Canvas>().enabled = false;
		UI_Loadout.GetComponent<Canvas>().enabled = true;
	}

	public void ClickExit(){
		#if UNITY_EDITOR
		// set the PlayMode to stop
		#else
		Application.Quit();
		#endif 
	}


	//TEXT HOVER

	public void HoverOnTextCol(GameObject text){
		text.transform.GetComponent<Text>().color = new Color(0,0,0);
	}
	public void HoverOffTextCol(GameObject text){
		text.transform.GetComponent<Text>().color = new Color(69,69,69);
	}

}
