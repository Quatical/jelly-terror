﻿using UnityEngine;
using System.Collections;

public class MoveToCam : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.transform.position = new Vector3 (this.transform.position.x, Camera.main.transform.position.y-1.5f, this.transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {

	}
}
