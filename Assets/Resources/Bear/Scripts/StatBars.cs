﻿using UnityEngine;
using System.Collections;

public class StatBars : MonoBehaviour {

	public EnergyBar HealthBarPlayer;
	public EnergyBar HealthBarCamera;
	public EnergyBar SerumBarCamera;

	public LimbSystem limbsystem;

	void Start() 
	{
		limbsystem = GetComponent<LimbSystem>();
	}

	void Update() 
	{
		HealthBarPlayer.valueCurrent = Mathf.RoundToInt(limbsystem.curHealth);
		//HealthBarCamera.valueCurrent = Mathf.RoundToInt(limbsystem.curHealth);
		//SerumBarCamera.valueCurrent = serum from somewhere...
	}
}
