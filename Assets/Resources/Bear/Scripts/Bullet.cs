﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public bool mayHit = false;
	public bool hit = false;
	public float ComboCooldown = 3f;
	private float Cooldown = 0;
	public int ComboCount = 0;

	public float limbDamage;
	public float overallDamage;
	private BoxCollider BC;
	private string name;
	private PlayMakerFSM ExpFSM;

	void Start () {
		name = transform.name;
		if (name == "OceanOloops") {
			mayHit = true;
		} else {
			PlayMakerFSM[] FSMs = transform.root.GetComponents<PlayMakerFSM>();
			foreach (PlayMakerFSM FSM in FSMs) {
				if (FSM.FsmName == "XP-Check") {
					ExpFSM = FSM;
					break;
				}
			}
		}
	}

	void Update () {
		if (Cooldown < 0) {
			ComboCount = 0;
			Cooldown = 0;
		} else {
			Cooldown -= Time.deltaTime;
		}
	}


	public void OnTriggerEnter (Collider other) {		
		if(transform.root != other.transform.root && mayHit)
		{
			LimbPart limb = other.gameObject.GetComponent<LimbPart>();
			if(limb != null && !hit)
			{	
				string Name = "";
				if (name != "OceanOloops") {
					Name = transform.root.GetComponent<PhotonView>().photonView.owner.name;
				} else {
					Name = "OceanOloops";
				}

				limb.DealDamage(this.tag, limbDamage, overallDamage, Name);
				if (name != "OceanOloops") {
					hit = true;
					ComboCount += 1;
					Cooldown = ComboCooldown;
					ExpFSM.Fsm.Variables.GetFsmInt("xp").Value += 5 * ComboCount;
				}
			}
		}
	}
}
