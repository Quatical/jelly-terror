﻿using UnityEngine;
using System.Collections;

public class LimbPart : MonoBehaviour {

	public GameObject shapeKeyObject;
	[System.NonSerialized]
	public SkinnedMeshRenderer skin;
	public int[] blendShapeIndex;
	public LimbSystem limbsystem;
	public GameObject[] limbsToSlice;
	public GameObject[] limbsToExplode;
	public GameObject[] limbTargets;
	public GameObject[] bloodTargets;
	public float maxHealth = 100;
	public float curHeatlh;
	public float curOverallDamage = 0;
	public PhotonView photonView;
	private bool LimbDestroyed = false;

	void Start() 
	{
		curHeatlh = maxHealth;
		photonView = PhotonView.Get(this);
	}

	public void SetShapeKey(float value) 
	{
		skin = shapeKeyObject.GetComponent<SkinnedMeshRenderer>();
		for(int i = 0; i < blendShapeIndex.Length; i++)
		{
			if(this.gameObject.name == "ORG-spine")
			{
				skin.SetBlendShapeWeight(0,0);
				skin.SetBlendShapeWeight(1,0);
				skin.SetBlendShapeWeight(2,0);
				skin.SetBlendShapeWeight(3,100);
				skin.SetBlendShapeWeight(4,0);
			}
			else
			{
				skin.SetBlendShapeWeight(blendShapeIndex[i], value);
			}
		}
	}

	public void ChopLimb()
	{
		SetShapeKey(100f);

		limbsystem.SpawnLimb(limbsToSlice, limbTargets);
		gameObject.GetComponent<Collider>().enabled = false;
		foreach(GameObject blood in bloodTargets)
		{
			blood.SetActive(true);
		}
	}

	public void ExplodeLimb()
	{
		SetShapeKey(100f);

		limbsystem.SpawnLimb(limbsToExplode, limbTargets);
		gameObject.GetComponent<Collider>().enabled = false;
		foreach(GameObject blood in bloodTargets)
		{
			blood.SetActive(true);
		}
	}

	public void DealDamage (string Tag, float limbValue, float overallValue, string PlayerName) {
		/*if (photonView.isMine) {
			return;
		}*/
		photonView.RPC ("WeaponCollision", PhotonTargets.All, new object[] { Tag, limbValue, overallValue, PlayerName });
	}

	[PunRPC]
	public void WeaponCollision (string Tag, float limbValue, float overallValue, string AttackerName, PhotonMessageInfo info) {
		SetHealth (Tag, limbValue, overallValue, AttackerName);
	}
	
	public void SetHealth(string Tag, float limbValue, float overallValue, string AttackerName)
	{
		if(limbsystem.curHealth > 0 && !limbsystem.dead)
		{
			float damage = curOverallDamage + overallValue;
			if (damage > 25) {
				limbsystem.curHealth -= damage - curOverallDamage;
				curHeatlh = 0;
				curOverallDamage = 25;
			} else {
				limbsystem.curHealth -= overallValue;
				curOverallDamage += overallValue;
			}
		}

		if(curHeatlh > 0)
		{
			curHeatlh -= limbValue;
		}
		if(curHeatlh <= 0 && !LimbDestroyed)
		{
			curHeatlh = 0;
			LimbDestroyed = true;

			if(Tag == "Slice")
			{
				ChopLimb();
			}

			
			if(Tag == "Explode")
			{
				ExplodeLimb();
			}

			/*if (name == "ORG-spine" || name == "ORG-Hips") {
				limbsystem.curHealth = 0;
			}*/
		}

		if(limbsystem.curHealth <= 0 && !limbsystem.dead)
		{
			limbsystem.Disable(AttackerName);
		}

		limbsystem.lastHitName = AttackerName;
	}
}
