﻿using UnityEngine;
using System.Collections;

public class RagdollController : MonoBehaviour {
	
	[System.NonSerialized]
	public SkinnedMeshRenderer skin;

	[System.NonSerialized]
	public SkinnedMeshRenderer ragdollSkin;

	public GameObject ragdollShapeKeyObject;

	public float timeToDestroy;

	public void SetShapeKeys(GameObject shape)
	{
		skin = shape.GetComponent<SkinnedMeshRenderer>();
		ragdollSkin = ragdollShapeKeyObject.GetComponent<SkinnedMeshRenderer>();

		for(int i = 0; i < skin.sharedMesh.blendShapeCount; i++)
		{
			ragdollSkin.SetBlendShapeWeight(i, skin.GetBlendShapeWeight(i));
		}

		Destroy(this.gameObject, timeToDestroy);
	}
}
