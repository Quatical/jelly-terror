﻿using UnityEngine;
using System.Collections;

public class DestroyMe : MonoBehaviour {

	public float time;

	void Start() 
	{
		Invoke ("Destroy",time);
	}

	void Destroy() 
	{
		Destroy(this.gameObject);
	}
}
