﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]

public class CharacterMovementController : MonoBehaviour  {

	private Animator animator;
	private Rigidbody rbody;
	private CapsuleCollider capsule;

	private float h;
	private float v;
	
	private bool camRotToggle;
	private GameObject cam;

	private Vector3 ContactPos;

	public bool isGrounded;
	public bool jumped;
	public bool jumpInput;
	public float jumpHeightMultiplier;
	public float jumpForwardMultiplier;

	public float SuperJumpHeightMultiplier;
	public float SuperJumpForwardMultiplier;

	public float CurrentJumpHeightMultiplier;
	public float CurrentJumpForwardMultiplier;

	public string serumType;

	public float heightTolerance;


	void Start() 
	{
		animator = GetComponent<Animator>();

		rbody = GetComponent<Rigidbody>();
		
		transform.position = animator.transform.position;
		
		camRotToggle = true;

		cam = GameObject.FindGameObjectWithTag("MainCamera");

		animator.applyRootMotion = true;
		SuperJumpForwardMultiplier = jumpForwardMultiplier * 1.5f;
		SuperJumpHeightMultiplier = jumpHeightMultiplier * 1.5f;

		CurrentJumpForwardMultiplier = jumpForwardMultiplier;
		CurrentJumpHeightMultiplier = jumpHeightMultiplier;
	}
	
	void FixedUpdate() 
	{
		transform.position = animator.transform.position;
		
		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");
		
		animator.SetFloat("Forward", v);
		animator.SetFloat("Turn", h);


		
		if(Input.GetKeyUp(KeyCode.LeftAlt) || Input.GetKeyUp(KeyCode.RightAlt))
		{
			camRotToggle = !camRotToggle;
		}
		
		if(!camRotToggle)
		{
			transform.rotation = animator.transform.rotation;
		}
		else
		{
			transform.Rotate(0,Input.GetAxis("Mouse X"),0);
		}
		
		if(Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
		{
			StartCoroutine(UseSerum(serumType));
		}
		if(Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
		{
			StopCoroutine(UseSerum(serumType));
		}

//		if(Input.GetButtonUp("Fire1"))
//		{
//			animator.SetTrigger("AttackTrigger");
//		}
//
//		if(Input.GetButtonUp("Fire2"))
//		{
//			animator.SetTrigger("DodgeTrigger");
//		}



		if(Input.GetKeyUp(KeyCode.Space))
		{
			if(isGrounded)
			{
				jumped = false;
				Jump();
			}
		}

		if(jumpInput)
		{
			//rbody.AddForce(0,jumpHeightMultiplier * Time.deltaTime,0,ForceMode.Force);
		}

		if(jumped)
		{
			//rbody.AddForce(0,-(jumpHeightMultiplier/2) * Time.deltaTime,0,ForceMode.Force);
		}
		
		if (!isGrounded)
		{
			if(!jumpInput)
			{
				animator.SetBool("OnGround", false);
				rbody.AddForce(transform.forward * CurrentJumpForwardMultiplier, ForceMode.Force);
			}
		}
		else
		{
			animator.SetBool("OnGround", true);
			jumped = false;
		}

	}


	void Jump() 
	{
		if(animator.GetBool("OnGround"))
		{
			animator.SetBool("JumpUp", true);
			jumpInput = true;
			isGrounded = false;
			rbody.AddForce(0,CurrentJumpHeightMultiplier, 0,ForceMode.Impulse);
		}
	}

	public void Jumped() 
	{
		animator.SetBool("JumpUp", false);
	}

	public void StopJumpForce()
	{
		jumpInput = false;
		jumped = true;
	}

	public void OnCollisionEnter(Collision collision) 
	{
		Collider col = gameObject.GetComponent<Collider>();

		if(collision.gameObject.tag == "Ground")
		{
			isGrounded = true;
			//Physics.IgnoreCollision(collision.collider, col);
		}
	}

	public void OnCollisionStay(Collision collision) 
	{
		Collider col = gameObject.GetComponent<Collider>();
		
		if(collision.gameObject.tag == "Ground")
		{
			isGrounded = true;
			ContactPos = collision.contacts[0].point;
			//Physics.IgnoreCollision(collision.collider, col);
		}
	}

	public void OnCollisionExit(Collision collision) 
	{
		Collider col = gameObject.GetComponent<Collider>();
		
		if(collision.gameObject.tag == "Ground")
		{
			Debug.Log(Vector3.Distance(transform.position, ContactPos));
			if(Vector3.Distance(transform.position, ContactPos) > heightTolerance) {
				isGrounded = false;
			}
			//Physics.IgnoreCollision(collision.collider, col);
		}
	}

	

	public IEnumerator UseSerum(string serumType) 
	{
		if(serumType == "Coffee")
		{
			animator.speed = 2f;
			CurrentJumpForwardMultiplier = SuperJumpForwardMultiplier;
			CurrentJumpHeightMultiplier = SuperJumpHeightMultiplier;
			heightTolerance = 0.7f;
			yield return new WaitForSeconds(15f);
			animator.speed = 1f;
			CurrentJumpForwardMultiplier = jumpForwardMultiplier;
			CurrentJumpHeightMultiplier = jumpHeightMultiplier;
			heightTolerance = 0.3f;
		}
		
		if(serumType == "SourBomb")
		{
			animator.speed = 2f;
			heightTolerance = 0.7f;
			CurrentJumpForwardMultiplier = SuperJumpForwardMultiplier;
			CurrentJumpHeightMultiplier = SuperJumpHeightMultiplier;
			yield return new WaitForSeconds(15f);
			animator.speed = 1f;
			CurrentJumpForwardMultiplier = jumpForwardMultiplier;
			CurrentJumpHeightMultiplier = jumpHeightMultiplier;
			heightTolerance = 0.3f;
		}
	}

	
}