﻿using UnityEngine;
using System.Collections;

public class LimbSystem : MonoBehaviour {

	public float minHealth = 0f;
	public float maxHealth = 100f;
	public float curHealth;
	public GameObject ragdoll;
	public GameObject shapeKeyObject;
	public bool dead = false;
	public string lastHitName = "";

	private BogusPlayerInfo playerStats;

	void Start() 
	{
		curHealth = maxHealth;
		playerStats = GetComponent<BogusPlayerInfo> ();
	}

	public void SpawnLimb(GameObject[] limbs, GameObject[] targets) 
	{
		for(int i = 0; i < limbs.Length; i++)
		{
			Instantiate(limbs[i], targets[i].transform.position, targets[i].transform.rotation);
		}
	}

	public void Disable(string KillerName) 
	{
		ragdoll = Instantiate(ragdoll, transform.position, Quaternion.identity) as GameObject;
		ragdoll.GetComponent<RagdollController>().SetShapeKeys(shapeKeyObject);

		playerStats.AddDeath ();
		playerStats.AddKill(FindPlayerWithName(KillerName));
		playerStats.UpdateScoresGlobally ();
		dead = true;
	}

	public PhotonPlayer FindPlayerWithName (string Name) {
		PhotonPlayer[] Players = PhotonNetwork.playerList;

		PhotonPlayer ReturnValue = null;
		foreach (PhotonPlayer Player in Players) {
			if (Player.name == Name) {
				ReturnValue = Player;
				break;
			}
		}
		return ReturnValue;
	}
}
